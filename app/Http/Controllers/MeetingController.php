<?php

namespace App\Http\Controllers;

use App\MeetingRoom;
use Illuminate\Http\Request;

class MeetingController extends Controller
{
    public function index()
    {
        $data = MeetingRoom::all();

        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    public function create()
    {
        $name = request()->name;
    }
}
