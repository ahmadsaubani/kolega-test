<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRoom extends Model
{
    protected $fillable = [
        'nama',
        'gambar',
        'jam',
        'harga'
    ];
}
